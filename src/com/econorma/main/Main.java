package com.econorma.main;
 

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.econorma.logic.ReadDBF;
import com.econorma.logic.ReadConfig;
import com.econorma.model.DateLabelFormatter;
  
 
public class Main extends JDialog implements ActionListener, KeyListener {
	
	private  NewJPanel panel;
	private static boolean consoleMode;
	public static String CONSOLE = "console";
	public static String PATH = "Percorso";
	public static String OK = "Conferma";
	public static String ANNULLA = "Annulla";
	public static String DATEFROM = "Data Da";
	public static String DATETO = "Data A";
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static void main(String args[]) {
		
		try {
			 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			 } catch (ClassNotFoundException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 } catch (InstantiationException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 } catch (IllegalAccessException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 } catch (UnsupportedLookAndFeelException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 }
		
		if (args.length == 0) {
			System.out.println("No arguments passed");
		} else {
			for (int i = 0; i < args.length; i++) {
				System.out.println("Arguments passed: " +  args[i]);
			}
		}
		
		if (args.length>0  && args[0].equals(CONSOLE)){
			consoleMode= true; 
			consoleMode();
		} else {
			Main dialog = new Main(null);
	  	    dialog.setVisible(true);
		}
	  
    }
 

	// private JComboBox filePath;

	public Main(JFrame parent) {
		super(parent);
	 	init();
  }
	
	 

	public void init(){

//		setSize(500, 150);
		setFocusable(true);
		setResizable(false);
		addKeyListener(this);

		setBounds(500, 300, 500, 180);
		setTitle("Export Vendite corrispettivo");
		super.dialogInit();
		 
		 

		panel = new NewJPanel();
		add(panel);

 
		panel.getOkButton().addActionListener(this);
		panel.getCancelButton().addActionListener(this);
		panel.getChoiceButton().addActionListener(this);
		
		
	}
	 
 
	 

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getSource() == panel.getOkButton()) {
			 
			
			if (panel.getfilePath().getText()==null || panel.getfilePath().getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Nessun file selezionato" ,"Errore", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); 
			
//			Date dateFrom = (Date) panel.getjDateFrom().getValue();
//			Date dateTo  = (Date) panel.getjDateFrom().getValue();
			
			try {
				
				String dataFromStr =  panel.getjDatePickerFrom().getJFormattedTextField().getText();
				String dataToStr =  panel.getjDatePickerTo().getJFormattedTextField().getText();
				 
				Date dateFrom = sdf.parse(dataFromStr);
			 	Date dateTo =  sdf.parse(dataToStr);
			 
				
				ReadConfig readConfig = new ReadConfig();
				Map<Integer, String> iva = readConfig.readIva();
				String path = readConfig.readPath();
				
				ReadDBF dbf = new ReadDBF();
				
					File f = new File(panel.getfilePath().getText());
					dbf.readDBF(f, dateFrom, dateTo, iva, path);
					
					JOptionPane.showMessageDialog(null, "Export corrispettivo eseguito con successo");
					
				}  
					catch (Exception e) {
					 System.out.println(e);
					 JOptionPane.showMessageDialog(null, "Export con errori: " + e);
				}
	 			
		}

		if (event.getSource() == panel.getCancelButton()) {
			dispose();
			System.exit(1);
		}
		
		if (event.getSource() == panel.getChoiceButton()) {
			
			 String percorso = (new File(".")).getAbsolutePath();
			 final JFileChooser jFileChooser = new JFileChooser(percorso);
			 
			final FileNameExtensionFilter filter =	 new FileNameExtensionFilter("Fox Pro database", "dbf");
			 jFileChooser.setFileFilter(filter); 
			 
		        int n = jFileChooser.showOpenDialog(this);
		        if (n == jFileChooser.APPROVE_OPTION) {
		        	
		        	final File f = new File(jFileChooser.getCurrentDirectory().toString()
		                    +"/"+jFileChooser.getSelectedFile().getName());
		        	
		        	String pippo = jFileChooser.getSelectedFile().getName();
	  
		        	panel.getfilePath().setText(jFileChooser.getSelectedFile().getAbsolutePath());
		        	
		       	 
		        }
			
			
		}
		
	}

	/**
	 * 
	 * @author marcobettiol
	 */
	private static class NewJPanel extends javax.swing.JPanel {

		/** Creates new form NewJPanel */
		public NewJPanel() {
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		public void initComponents() {
			
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			jLabel1 = new javax.swing.JLabel();
			filePath = new javax.swing.JTextField();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();
			jButton3 = new javax.swing.JButton();
			jLabel2 = new javax.swing.JLabel();
			jDateFrom = new javax.swing.JFormattedTextField(format);
			jLabel3 = new javax.swing.JLabel();
			jDateTo = new javax.swing.JFormattedTextField(format);
			
			UtilDateModel modelFrom = new UtilDateModel();
			UtilDateModel modelTo = new UtilDateModel();
			Properties p = new Properties();
			p.put("text.today", "Today");
			p.put("text.month", "Month");
			p.put("text.year", "Year");
			JDatePanelImpl datePanelFrom = new JDatePanelImpl(modelFrom, p);
			JDatePanelImpl datePanelTo = new JDatePanelImpl(modelTo, p);
			// Don't know about the formatter, but there it is...
			jDatePickerFrom = new JDatePickerImpl(datePanelFrom, new DateLabelFormatter());
			jDatePickerTo = new JDatePickerImpl(datePanelTo, new DateLabelFormatter());
		 
		 
			 
		 	Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, -1);  
//			jDateFrom.setValue(new String(format.format(c.getTime())));
//			jDateTo.setValue(new String(format.format(c.getTime())));
			
			jDateFrom.setValue(c.getTime());
			jDateTo.setValue(c.getTime());
			
			setName("Form"); // NOI18N
			 
			jLabel1.setText(PATH);
			jLabel2.setText(DATEFROM);
			jLabel3.setText(DATETO);
			filePath.setEnabled(false);


			jButton1.setText(OK); // NOI18N

			jButton2.setText(ANNULLA); // NOI18N
			jButton3.setText("..."); 
			
			org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
					this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							org.jdesktop.layout.GroupLayout.LEADING)
					.add(layout
							.createSequentialGroup()
							.add(20, 20, 20)
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.LEADING)
									.add(layout
											.createSequentialGroup()
											.add(jLabel1,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
													100,
													org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													org.jdesktop.layout.LayoutStyle.RELATED)
											.add(filePath, 0, 320,
													Short.MAX_VALUE)
													.add(2, 2, 2)
													.add(jButton3)
											)
									
										.add(org.jdesktop.layout.GroupLayout.TRAILING,
											layout.createSequentialGroup()
													.add(jLabel2)
													.add(jDatePickerFrom)
													.add(18, 18, 18)
													.add(jLabel3)
													.add(jDatePickerTo)
												)
											
											
											
									.add(org.jdesktop.layout.GroupLayout.TRAILING,
											layout.createSequentialGroup()
													.add(jButton1)
													.add(18, 18, 18)
													.add(jButton2)))
							.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							org.jdesktop.layout.GroupLayout.LEADING)
					.add(layout
							.createSequentialGroup()
							.addContainerGap()
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jLabel1,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
											40,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
									.add(filePath,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
											org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
											org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
										.add(jButton3)		
									)
							.addPreferredGap(
									org.jdesktop.layout.LayoutStyle.RELATED)
									
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jLabel2).add(jDatePickerFrom).add(jLabel3).add(jDatePickerTo))
									
									.addPreferredGap(
									org.jdesktop.layout.LayoutStyle.RELATED)
									
							.add(layout
									.createParallelGroup(
											org.jdesktop.layout.GroupLayout.BASELINE)
									.add(jButton2).add(jButton1))
							.addContainerGap(
									org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)
							
							.addPreferredGap(
									org.jdesktop.layout.LayoutStyle.RELATED)		
							
							));
		}// </editor-fold>
			// Variables declaration - do not modify
		
		
		 

		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JButton jButton3;
		private javax.swing.JTextField filePath;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JLabel jLabel3;
		private javax.swing.JFormattedTextField jDateFrom;
		private javax.swing.JFormattedTextField jDateTo;
		private JDatePickerImpl jDatePickerFrom;
		private JDatePickerImpl jDatePickerTo;

		// End of variables declaration

		public JTextField getfilePath() {
			return filePath;
		}

		public JButton getOkButton() {
			return jButton1;
		}

		public JButton getCancelButton() {
			return jButton2;
		}
		public JButton getChoiceButton() {
			return jButton3;
		}

		public JFormattedTextField getjDateFrom() {
			return jDateFrom;
		}

		public void setjDateFrom(JFormattedTextField jDateFrom) {
			this.jDateFrom = jDateFrom;
		}

		public JFormattedTextField getjDateTo() {
			return jDateTo;
		}

		public void setjDateTo(JFormattedTextField jDateTo) {
			this.jDateTo = jDateTo;
		}

		public JDatePickerImpl getjDatePickerFrom() {
			return jDatePickerFrom;
		}

		public void setjDatePickerFrom(JDatePickerImpl jDatePickerFrom) {
			this.jDatePickerFrom = jDatePickerFrom;
		}

		public JDatePickerImpl getjDatePickerTo() {
			return jDatePickerTo;
		}

		public void setjDatePickerTo(JDatePickerImpl jDatePickerTo) {
			this.jDatePickerTo = jDatePickerTo;
		}
		
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE)  
		{  
			dispose(); 
		}  
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static Calendar getPreviousDate(){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -1);  
		return c;
	}
	
	
	public static void consoleMode(){
//		 Date date = null;
//		 
//		 try 
//		    {  
//		      String datestr="01/09/2016";
//		      SimpleDateFormat formatter; 
//		     
//		      formatter = new SimpleDateFormat("dd/MM/yyyy");
//		      date = (Date)formatter.parse(datestr);  
//		    } 
//		    catch (Exception e)
//		    {}
		
		Calendar cal = Calendar.getInstance(); // locale-specific
		cal.setTime(getPreviousDate().getTime());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		Date dateFrom = cal.getTime();
		Date dateTo  = cal.getTime();
		
		ReadConfig readConfig = new ReadConfig();
		Map<Integer, String> iva = readConfig.readIva();
		String path = readConfig.readPath();
	 	
		ReadDBF dbf = new ReadDBF();
		try {
			File f = new File("CASTIVA.DBF");
			dbf.readDBF(f, dateFrom, dateTo, iva, path);
		}  
			catch (Exception e) {
			 System.out.println(e);
		}
	}
	
	 
	 
}
