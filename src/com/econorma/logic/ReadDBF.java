package com.econorma.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.econorma.data.Corrispettivo;
import com.econorma.dbf.core.DbfMetadata;
import com.econorma.dbf.core.DbfRecord;
import com.econorma.dbf.reader.DbfReader;
import com.econorma.util.StringUtils;



public class ReadDBF {

	private SimpleDateFormat exportSdf = new SimpleDateFormat("yyyyMMdd");
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


	public void readDBF(File file, Date dateFrom, Date dateTo, Map<Integer, String> iva, String path) throws IOException, ParseException {

		Map<Corrispettivo, Integer> corrispettivo = new LinkedHashMap<Corrispettivo, Integer>();

		Calendar calFrom = Calendar.getInstance();
		calFrom.clear();
		calFrom.set(Calendar.MONTH, dateFrom.getMonth());
		calFrom.set(Calendar.YEAR, dateFrom.getYear());
		calFrom.set(Calendar.DAY_OF_MONTH, dateFrom.getDay());
		calFrom.set(Calendar.HOUR, 0);
		calFrom.set(Calendar.MINUTE, 0);
		calFrom.set(Calendar.SECOND, 0);
		calFrom.set(Calendar.MILLISECOND, 0);

		File out = new File(path + "CORRIS.TXT");
		FileWriter fw = new FileWriter(out,true);


		Charset stringCharset = Charset.forName("Cp866");

		InputStream dbf = new FileInputStream(file.toString());

		DbfRecord rec;
		try (DbfReader reader = new DbfReader(dbf)) {
			DbfMetadata meta = reader.getMetadata();
			Integer sumValue = 0;
			Date dataStored = null;


			System.out.println("Read DBF Metadata: " + meta);
			while ((rec = reader.read()) != null) {
				rec.setStringCharset(stringCharset);

				Map<String, Object> map = rec.toMap();


				Date dataDbf = (Date) map.get("DATA_IVA");
//				System.out.println("Record :" + rec.getRecordNumber() + ": " + sdf.format(dataDbf));
//				if (sdf.format(dataDbf).equals("2016/09/13")){
//					boolean ok = true;
//				}
		 
				
//			if(dateFrom.equals(dataDbf)){
				
				if (dataDbf.before(dateFrom) || dataDbf.after(dateTo)){
					continue;
				}
				
				{

					BigDecimal codIva = (BigDecimal) map.get("COD_IVA");
					BigDecimal alIva = (BigDecimal) map.get("ALI_IVA");
					BigDecimal importo = (BigDecimal) map.get("IMP_IVA");
					String desIva = (String) map.get("DES_IVA");
					System.out.println("Record :" + rec.getRecordNumber() + ": " + sdf.format(dataDbf) + "|" + codIva + "|" + alIva + "|" + importo);


					String ricodIva = iva.get(codIva.intValue());
 
					Corrispettivo cor = new Corrispettivo();
					cor.setData(exportSdf.format(dataDbf));
					cor.setNegozio(07);
					cor.setCassa(1);
					cor.setTipo(ricodIva);

					if (corrispettivo.get(cor) != null){
						corrispettivo.put(cor, corrispettivo.get(cor) +  importo.intValue());
					} else {
						corrispettivo.put(cor, importo.intValue());
					}

					sumValue+= importo.intValue();
					dataStored = dataDbf;

				}

				 
			}

			if (dataStored != null) {
				for (int i= 0; i < 2; i++ ) {
					String tipo = null;
					switch (i) {
					case 0:
						tipo = "0401";	
						break;
					case 1:
						tipo = "0445";
						break;
					default:
						break;
					}
					Corrispettivo cor = new Corrispettivo();
					cor.setData(exportSdf.format(dataStored));
					cor.setNegozio(07);
					cor.setCassa(1);
					cor.setTipo(tipo);
					corrispettivo.put(cor, sumValue);
				}
			}
	
			for (Map.Entry<Corrispettivo, Integer> entry : corrispettivo.entrySet()) {

				String dataStr = StringUtils.fixedLengthString(entry.getKey().getData(), 8);
				String ivaStr = StringUtils.fixedLengthString(entry.getKey().getTipo(), 4);
				String importoStr = StringUtils.fixedLengthString(String.valueOf(entry.getValue()), 10);
				String negozioStr = StringUtils.fixedLengthString(String.valueOf(entry.getKey().getNegozio()), 2);  
				String cassaStr = StringUtils.fixedLengthString(String.valueOf(entry.getKey().getCassa()), 1); 

				fw.write(dataStr + negozioStr + cassaStr + ivaStr + importoStr);
				fw.write("\r\n");

				System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			}


			fw.close();

		}
	}




}
