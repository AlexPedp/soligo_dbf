package com.econorma.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadConfig {


	public Map<Integer, String>  readIva() {

		Map<Integer, String> map = new HashMap<Integer, String>();
		StringBuilder sb = new StringBuilder();

		File file = new File("tabelle.txt");
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);

			int content;
			while ((content = fis.read()) != -1) {
				sb.append((char) content);

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		//		System.out.println(sb.toString());


		try {

			String[] lines = sb.toString().split("\\n");
			for(String s: lines){

				if (s.contains("IVA")){
					
					int iva = Integer.parseInt(s.substring(3, 5).trim());
					String value =  s.substring(5, 9);
					
					map.put(iva, value);
				}

			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}



		return map;
	}
	
	
	public String  readPath() {

		 
		StringBuilder sb = new StringBuilder();

		File file = new File("path.txt");
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);

			int content;
			while ((content = fis.read()) != -1) {
				sb.append((char) content);

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		//		System.out.println(sb.toString());

		String path = null;

		try {

			path = sb.toString();			
		 
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}



		return path;
	}


}
