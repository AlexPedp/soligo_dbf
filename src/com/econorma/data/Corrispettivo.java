package com.econorma.data;

public class Corrispettivo  {

	 public String data;
	 public int negozio;
	 public int cassa;
	 public String tipo;
	  
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getNegozio() {
		return negozio;
	}
	public void setNegozio(int negozio) {
		this.negozio = negozio;
	}
	public int getCassa() {
		return cassa;
	}
	public void setCassa(int cassa) {
		this.cassa = cassa;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof Corrispettivo) {
        	Corrispettivo s = (Corrispettivo)obj;
            return data.equals(s.data) && tipo.equals(s.tipo);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (data + tipo).hashCode();
    }
    

}
